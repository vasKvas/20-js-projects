const video = document.getElementById('video');
const play = document.getElementById('play');
const stop = document.getElementById('stop');
const sound = document.getElementById('sound');
const progress = document.getElementById('progress');
const timestamp = document.getElementById('timestamp');

console.log(video.muted);

function toggleVideoStatus() {
  if(video.paused){
    video.play();
  } else {
    video.pause();
  }
}
function updatePlayIcon() {
  play.classList.toggle('run');
}

function stopVideo() {
  video.currentTime = 0;
  video.pause();
}

function updateProgress() {
  progress.value = (video.currentTime / video.duration) * 100;

  let mins = Math.floor(video.currentTime / 60);
  if(mins < 10) {
    mins = `0${mins}`;
  }

  let secs = Math.floor(video.currentTime % 60);
  if(secs < 10) {
    secs = `0${secs}`;
  }

  timestamp.innerText = `${mins}:${secs}`;
}

function setVideoProgress() {
  video.currentTime = (+progress.value * video.duration) / 100;
}

function toggleSoundStatus() {
  video.muted = !video.muted;
  sound.classList.toggle('muted');
}


//Event listeners
video.addEventListener('click', toggleVideoStatus);
video.addEventListener('play', updatePlayIcon);
video.addEventListener('pause', updatePlayIcon);
video.addEventListener('timeupdate', updateProgress);

play.addEventListener('click', toggleVideoStatus)
stop.addEventListener('click', stopVideo);
sound.addEventListener('click', toggleSoundStatus);
progress.addEventListener('change', setVideoProgress);
