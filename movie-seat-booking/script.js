const container = document.querySelector(".container");
const movieSelector = document.getElementById("movie");
const seatsCount = document.getElementById("count");
const totalPrice = document.getElementById("total");
const seats = document.querySelectorAll(".row .seat:not(.occupied)");
let ticketsPrice = +movieSelector.value;

populateUI();

function saveMovieData(movieIndex, moviePrice) {
  localStorage.setItem("movieIndex", movieIndex);
  localStorage.setItem("moviePrice", moviePrice);
}

function updateSelectedCount() {
  const seatsSelected = document.querySelectorAll(".row .selected");
  const seatsIndex = [...seatsSelected].map((seat) => [...seats].indexOf(seat));
  localStorage.setItem("seatsIndex", JSON.stringify(seatsIndex));

  seatsCount.innerText = seatsSelected.length;
  totalPrice.innerText = seatsSelected.length * ticketsPrice;
}

function populateUI() {
  const selectedSeats = JSON.parse(localStorage.getItem("seatsIndex"));
  const selectedMovie = localStorage.getItem("movieIndex");
  if (selectedSeats !== null && selectedSeats.length > 0) {
    seats.forEach(function (seat, index) {
      if (selectedSeats.indexOf(index) > -1) {
        seat.classList.add("selected");
      }
    });
  }

  if (selectedMovie !== null) {
    movieSelector.selectedIndex = selectedMovie;
  }
  updateSelectedCount();
}

movieSelector.addEventListener("change", (e) => {
  ticketsPrice = +e.target.value;
  saveMovieData(e.target.selectedIndex, e.target.value);
  updateSelectedCount();
});

container.addEventListener("click", (e) => {
  if (
    e.target.classList.contains("seat") &&
    !e.target.classList.contains("occupied")
  ) {
    e.target.classList.toggle("selected");
    updateSelectedCount();
  }
});
